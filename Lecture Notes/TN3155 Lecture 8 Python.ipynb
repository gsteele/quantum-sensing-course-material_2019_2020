{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from qutip import *\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams['figure.dpi'] = 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Decay of a spin with the Lindblad master equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we will use QuTiP to solve the problem discussed in class of the decay of a spin due to the emission of a photon. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up the problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will first create initial state in the 2-dimensional Hilbert space. \n",
    "\n",
    "Note that `basis(N,0)` is completely identical to the function `fock(N,0)` we used earlier. We use this one here just because it is (maybe) strange to call the levels of a spin \"Fock\" states.\n",
    "\n",
    "In the last line, we will print out the state we created to make sure it is what we think it is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2], [1]], shape = (2, 1), type = ket\\begin{equation*}\\left(\\begin{array}{*{11}c}1.0\\\\0.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2], [1]], shape = (2, 1), type = ket\n",
       "Qobj data =\n",
       "[[1.]\n",
       " [0.]]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "N = 2\n",
    "psi0 = basis(N,0)\n",
    "psi0 = psi0.unit()\n",
    "psi0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we need an operator with the following matrix for our \"collapse\" opeartor, which is the \n",
    "\n",
    "$$\n",
    "\\hat \\sigma_{-} = \n",
    "\\begin{pmatrix}\n",
    "0 & 0 \\\\\n",
    "1 & 0 \n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "This is identical to the \"creation\" operator, as it turns out, for the harmonic oscillator (HO). (This is because spin-up has higher energy but it is the first row of the basis state, whereas the first row for the HO is the ground state.). \n",
    "\n",
    "But, again, it would be confusing to write code for a spin that uses HO operators. Fortunately, QuTiP has a built-in functions to generate the Pauli matrices:\n",
    "\n",
    "http://qutip.org/docs/latest/apidoc/functions.html#qutip.operators.sigmam\n",
    "\n",
    "We will use `sigmam()` to make the annihilation operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = False\\begin{equation*}\\left(\\begin{array}{*{11}c}0.0 & 0.0\\\\1.0 & 0.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = False\n",
       "Qobj data =\n",
       "[[0. 0.]\n",
       " [1. 0.]]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sig_m = sigmam()\n",
    "sig_m"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And, we can double-check that it does what we think it should: \n",
    "\n",
    "$$\n",
    "\\hat \\sigma_- \\left| \\uparrow \\right\\rangle = \\left| \\downarrow \\right\\rangle\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2], [1]], shape = (2, 1), type = ket\\begin{equation*}\\left(\\begin{array}{*{11}c}0.0\\\\1.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2], [1]], shape = (2, 1), type = ket\n",
       "Qobj data =\n",
       "[[0.]\n",
       " [1.]]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sig_m*psi0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Good, this is indeed what we expect! \n",
    "\n",
    "Now, we need the Hamiltonian. For the Hamiltonian, we will pick just the $\\sigma_z$ operator:\n",
    "\n",
    "$$\n",
    "\\hat H = \\tfrac{1}{2} \\hat \\sigma_z\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = True\\begin{equation*}\\left(\\begin{array}{*{11}c}0.500 & 0.0\\\\0.0 & -0.500\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = True\n",
       "Qobj data =\n",
       "[[ 0.5  0. ]\n",
       " [ 0.  -0.5]]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H = 0.5 * sigmaz()\n",
    "H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This implicitly means that we are choosing a Larmor frequency:\n",
    "\n",
    "$$\n",
    "\\omega_L = \\frac{1}{\\hbar} = 2\\pi \\times \\frac{1}{h} = 2\\pi \\times 1.5 \\times 10^{33}\\  {\\rm Hz}\n",
    "$$\n",
    "\n",
    "The may seem mega-fast ($10^{33}$ Hz), and it is! It turns out, however, that when QuTiP solves time-dependent Hamiltonians, it rescales the time-variables in the problem such that $\\hbar = 1$. Our  choice of $\\hat H  = \\frac{1}{2}\\hat \\sigma_z$ leads to an angular Larmor frequency of:\n",
    "\n",
    "$$\n",
    "\\omega_L = 1\\ {\\rm rad / sec} = 0.15\\ {\\rm Hz}\n",
    "$$\n",
    "\n",
    "in \"QuTiP\" variables. This is not a big deal: it just changes the rescaling of the time axis of any plots. But one should be careful to remember what the time axis means when performing a simulation. \n",
    "\n",
    "Even more convenient for plotting, though, is to choose $\\omega_L = 2\\pi$ so that the oscillations we will see, for example in $S_x$ for a superposition state will have a period of 1 second (1 time unit):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = 1 * 2 * np.pi \n",
    "H = 0.5 * w * sigmaz()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solving the master equation\n",
    "\n",
    "We now have everything we need to solve the (Lindblad) master equation!\n",
    "\n",
    "http://qutip.org/docs/latest/guide/dynamics/dynamics-master.html\n",
    "\n",
    "To do this, we need to use the qutip function `mesolve()`: \n",
    "\n",
    "http://qutip.org/docs/latest/apidoc/functions.html#module-qutip.mesolve\n",
    "\n",
    "For QuTiP to solve the master equation, we need to give it a few thigns:\n",
    "\n",
    "* `H`: the Hamiltonian\n",
    "* `psi0`: the quantum state at $t=0$ (density matrix or wave function)\n",
    "* `t`: A list of times for which you would like the solution\n",
    "* `c_ops`: A python \"list\" of collapse operators\n",
    "\n",
    "For our collapse operator, we will need to scale the $\\sigma_-$ operator by $\\sqrt{\\gamma}$, where $\\gamma$ is the average rate that the collapse is applied (the average number of collapses that occur per second):\n",
    "\n",
    "$$\n",
    "\\hat C = \\sqrt{\\gamma}\\ \\hat \\sigma_-\n",
    "$$\n",
    "\n",
    "Like $\\omega_L$, the rate $\\gamma$ has the units equivalent to 1 / second. If we choose $\\gamma = 2\\pi \\times 0.1 $, then with our choice of $\\hat H = \\frac{1}{2} \\hat \\sigma_z$, how many oscillations will we see in $\\sigma_x$ for exmaple if we started with $|x+\\rangle$? For a function $f(x) = e^{-\\gamma t/2} \\cos(\\omega_t)$, we would then observe $10/(2\\pi) = 1.59$ oscillations by the time the amplitude decays by $1/e$. \n",
    "\n",
    "Since we are performing only one measurement, we all we have to do is put this operator in square brackets to create our `c_ops` python list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "gamma = 0.1 * w\n",
    "c_ops = [np.sqrt(gamma) * sig_m]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we need to create the time array at which we want QuTiP to give us the solution of the master equation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = np.linspace(0, 10, 401) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And then we can just call `mesolve()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = mesolve(H, psi0, t, c_ops)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we will use the projection operator to extract values of the density matrix to plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAhoAAAFtCAYAAABBdsPCAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAPYQAAD2EBqD+naQAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8QZhcZAAAgAElEQVR4nOzdd5wV1f3/8de5d3vfZXdhKUtZunQUAUEldkxUbNhjiCWSZkw00XzN95d8E01ibEksMZpI7BpbVFSiFAXpvfeytK2wvd/z+2N2YYEFd2HvnXt338/HYx4zd2buzIdV2bdzzpxjrLWIiIiI+IPH7QJERESk7VLQEBEREb9R0BARERG/UdAQERERv1HQEBEREb9R0BARERG/UdAQERERv1HQEBEREb8Jc7sANxljDNAZKHG7FhERkRAUD+y1Jxj9s10HDZyQsdvtIkREREJYV2DP8Q6296BRApCdnU1CQoLbtYiIiISM4uJiunXrBl/TKtDegwYACQkJChoiIiJ+oM6gIiIi4jcKGiIiIuI3ChoiIiLiNwoaIiIi4jcKGiIiIuI3ChoiIiLiN0ERNIwxZxtjPjDG7DXGWGPMFc34zjnGmKXGmEpjzDZjzPcCUauIiIg0X1AEDSAWWAn8oDknG2N6AtOBL4HhwEPAn40xV/mtQhEREWmxoBiwy1r7MfAxgDP9yNf6HrDLWnt3/ef1xpjTgZ8Bb/ulSBEREWmxoAgaJ2EMMOOofZ8C3zXGhFtra5r6kjEmEohstCu+NYtas6eIt5Zk0y0lhtvG92rNS4uIiDTNWrA+8NWBrXO2j1jqj4dHQ0RswMsL1aDRCcg5al8Ozp8nFdh3nO/dD/yvv4ramlfKtPk7GdQlQUFDRMTf6mqhrgrqqqG22lnXVYOvFupqnHXD4pfPdeBr2N/oF3zDL/xD++rA52tiX12j8xvva3T+Eddral8dcNyJU4/0jf+Bs+/16z+SpoRq0IBjf7LmOPsbexh4rNHneFpx9tYxvToAsHZvMUXlNSTGhLfWpUVEgoe1zi/0mgqorWze+oh9VUcFhCrnF3dt1eGwcER4aHy85vB3rc/tn0RoOf5M7n4VqkFjP85TjcbSgVqg4HhfstZWAVUNn5vZH6TZ0hOi6JUWy7a8MhbtKOSCgR1b9foiIi3iq4Oacqguq19Koar08Hbj/Ye2mzhWU35sWGju/0UHkjcSvBHgDQdPWP3aC57mfg5z1sf77PEevrYn/MjPxgvGOPuM9/DaeOq3G60bH/d4jt1nPPX7T3Sdo7Y99fc/+twjltb9nddcoRo05gPfOmrfhcCS4/XPCJTRvTqwLa+M+VsLFDRE5ORZ6/yCryyCymJnXVW/PmK78bGjzqsuDUChxmn7D4s6wToKwqKPXHsjISyiPhhEOr+wwxqCQkT9dvjh8BAWcdR2xJHnesJc+0UqJxYUQcMYEwf0brSrpzFmGFBord1ljHkY6GKtvaX++LPAD4wxjwF/x+kc+l3g+kDW3ZQxvTrw6sJdzN923AcrItLe+HxQeRAqDkB5IVQUQnlBo+1G68bbdVVff+3mMB6IiHM6Ah5aTvQ5rtHnOCcwNCxHBwlvhH7BywkFRdAATgdmNfrc0I9iGnArkAFkNhy01m43xkwEHge+D+wFfmStdf3V1tH1/TTW7yvmQFk1ybERLlckIn7h8znBoSwXSnOgNO/I7dKc+s95UJbndNw7GcYLUQkQlQiR9esjthsfO+q8yASIjHNCgcKAuCQogoa1djaHO3M2dfzWJvbNAUb4r6qTkxYfSZ/0ODbnlrJweyEXDzq6K4mIBL2aSijZC8UNy54jt0v2O+HBV9uy60bEQXQKxNQvh7Y7HN6OTm60L9n5jkKChLCgCBptzeheHdicW8qCbQUKGiLBxlqnWeLgDjiwEw7uhIO7jgwU5S1o+oxOhth0iKtfjrcd08HpSyDSziho+MGYrA68tGAn87eqn4aIK2qroHAbFG53gkRDoGhYN6eTZFg0JHSuX7ocuR3fCeI6Qmya0zFRRI5LQcMPzuyZAsDGnBIKSqvoEKf/ixFpdT4fFO+Ggi1QsBXyN9dvb3GeUHzd65dxnSC5OyR1h6RMSOx6ZKCITlaThUgrUNDwgw5xkfTrGM/GnBIWbi9k4uAMt0sSCV0+n9PMkbsectfVrzdA4VZnTIfjiUyAlJ5OkGgIFMk96oNFN+eNCRHxOwUNPxmT1YGNOSXM31qgoCHSXCU5kLOmPkysh9y1kLfRGU+iKZ5wSOkFHXpDhyxI7VO/3dtp1tATCRHXKWj4yeheHXjxqx0aT0OkKdY6HS/3rYS9K5z1vpVQur/p872RkNYP0gdCx4GQ1t8JFYmZzuiNIhK09F+on4zulYIxsCW3lLySKtLi1U9D2rHSPNi9CHYvORwqyvOPPc94ICXLCRPpp0H6ACdcpPR0hlQWkZCjoOEnSTER9O+UwPp9xSzYVsC3hnZ2uySRwKirdZo8shfB7sXO+sD2Y8/zhEHaAMgY6iydh0HH01yZxlpE/EdBw4/G9OrA+n3FzFfQkLaspgKyF8KOubBrAexZBjVlx56X1h+6ngGdhzuhIv00Z84LEWnTFDT8aExWB/4xbzsLNJ6GtCU1Fc6Tiu1fOuFizxJnyu7GIhOg6+nQdRR0OwO6nA7RSe7UKyKuUtDwo1E9U/AY2JZfxr6iCjIS9TqdhCCfD/avgi2fwdZZTl+Lo4NFfGfoOR4yx0C3M52nFx6PO/WKSFBR0PCjxOhwhnRNYkX2Qb7cnM+1p3dzuySR5inLh60z68PFTGdej8biM6DHeOgxzllSeulVUhFpkoKGn43rncqK7IPMVdCQYGYt7F8NGz6CzZ86r5w2HlkzIg56ngO9vwG9JihYiEizKWj42bg+qfx11hbmbcnH57N4PPrLWYJEXS3smu+Eiw0fQdGuI493Ggy9z3eWrqM0p4eInBQFDT8bkZlMTISXgrJq1u8v5rTOiW6XJO1ZbRVs+Rw2fAgbP4aKwsPHwqKh93nQb6KzjtfMwyJy6hQ0/CwizMOZPVOYtTGPuZvzFTQk8OpqYfscWPMOrP8AqooOH4tOdoJF/0udJpGIGPfqFJE2SUEjAMb1SXOCxpZ87jwny+1ypD2w1hnTYs3bsO69IztzxneGgZdB/286b4loCG8R8SP9DRMA4/ukArBoeyGVNXVEhWsoZfGTot2w4jVY8cqRo3FGp8BpV8CgqyBzrF49FZGAUdAIgD7pcXRMiCSnuIqlOw9wVu9Ut0uStqSmEjZ+BMtfdsa5aHhbJCLOeWox+GrodS54w10sUkTaKwWNADDGcFbvVN5ZtocvN+craEjryNsES16Ala9D5cHD+7uPg+E3Oc0jmjdERFymoBEg4/s4QWPuljygv9vlSKiqq4VNH8OivzsdPBskdIVh18OwG5wxLkREgoSCRoA0PMVYu7eYwrJqUmI1JoG0QGkeLHsRlrwIxbudfcYDfS+G078LWRM0jbqIBCUFjQBJj4+if6d4NuwvYd6WfM3mKs2Tvxm++rPTPNIwv0hMBxhxC5w+BZIy3a1PRORrKGgE0LjeqWzYX8LczQoa8jWyF8G8J50ROxs6d3YZCaPugIFXaHp1EQkZChoBdFafVJ6fu525W/Kx1mI0V4Q05vM584zMe9IZGrxBv0vhrB9B5mj3ahMROUkKGgF0Zs8UIrwe9hysYFt+GVlpcW6XJMHA53OGBJ/zB8hZ4+zzRsDQ62DMDyGtr7v1iYicAgWNAIqJCOP0Hsl8tbWAORvzFDTau6YCRmQCnPFdOPN7mmtERNoEBY0AO7dfGl9tLWD2pjymjOvpdjniBmth43SY9TDkrHb2RSbA6LucJTrZ3fpERFqRgkaATeiXzkPTN7BgWwEV1XVER+iVxHZl1wL47/9C9gLnc0T84YARk+JubSIifqCgEWC90+PokhTNnoMVzN+Wzzf6d3S7JAmEvI3w2a+docLBmZJ99F0w9ocKGCLSpmlmpQAzxnBuvzQAZm/M+5qzJeSV5sEHP4anRzshw3icMTB+tAzO/1+FDBFp8/REwwXn9kvnlYW7mLUxV6+5tlV1NbD4BZj1EFQVOfv6XeqEi7R+7tYmIhJAChouGJvVgQivh+xCvebaJm2bDR//AvLWO587DYFL/gDdx7paloiIGxQ0XBAbGcaoninM3ZLPbL3m2nYU7YZP7of1/3E+R6fAeb9ymko0D4mItFPqo+GSw/00cl2uRE6Zrw4W/g2eOtMJGcYLo+50+mGc/h2FDBFp1xQ0XHJuv3QAFm4rpLy61uVq5KTlrIUXLoSP74PqUuh2JnzvS5j4R42HISKCgoZrstJi6ZocTXWdj/lbC9wuR1qqpsJ5XfVvZ8OeJc6AW5c+Ct/5BDqe5nZ1IiJBQ0HDJcYYJtQ/1Zil5pPQsmepEzDmPga+WhjwLfj+QjjjNvDoPykRkcb0t6KLGo+nYa11uRr5WnU1zuuqz18A+ZsgrhNMfgUmvwwJnd2uTkQkKOmtExeNyepARJiH3Qcq2JpXSu/0eLdLkuPJ3QDv3gH7VjqfB10NEx/RgFsiIl9DTzRcFBMRxpk9nV9UMzeo+SQo+Xww/ymnqWTfSqeD59X/gKtfUMgQEWkGBQ2XnT/Amevks/UKGkGnrABeuw4+fQDqqqD3BXDXfBh0lduViYiEDAUNl503wOkQumRHIQfKql2uRg7Z+RU8Ow42fwreSOeNkhvfgoQMtysTEQkpChou65ocw4CMBHxWb58EBV8dfPEIvHgplOyFDn3g9s+dN0o0J42ISIsFVdAwxkw1xmw3xlQaY5YaY8Z/zfl3G2M2GmMqjDHZxpjHjTFRgaq3tVxQ/1Tjs/U5LlfSzpXlw8tXwszfgvXBkOvgjtnQabDblYmIhKygCRrGmMnAE8DvgOHAl8DHxpjM45x/I/B74NfAAOC7wGTg4YAU3IrOH+j005izMY+q2jqXq2mn9q6A5851JkQLj4HLn4ZJz0Kk5qERETkVQRM0gHuAF6y1z1tr11tr7waygbuOc/4YYJ619lVr7Q5r7QzgNeD0ANXbagZ1TqRjQiRl1XUaJdQNq96Ef1wERdmQ0gtunwnDb1RTiYhIKwiKoGGMiQBGAjOOOjQDON7c2nOBkcaYUfXX6AVMBD46wX0ijTEJDQsQFANXeDyG8w69faLmk4Cpq4VPfwnv3A61lc5bJbfPgvQBblcmItJmBEXQAFIBL3D0b9kcoFNTX7DWvg48CMw1xtQAW4FZ1trfn+A+9wNFjZbdp1h3q7mgIWisy9UooYFQXuj0x5j/V+fz+J/CDW9AdJK7dYmItDHBEjQaHP0b1jSxzzlgzLnAL4GpwAjgSuCbxpgHT3D9h4HERkvXU6y31YzJ6kB0uJf9xZWs3VvsdjltW8FWeP582D7H6Y9xzYtw3q80nbuIiB8ES9DIB+o49ulFOsc+5Wjwf8BL9X06Vltr3wUeAO43xjT557LWVllrixsWoKSV6j9lUeFezu6bCsB/16n5xG+yF8ELF0DhVkjsBrd9BqdNcrsqEZE2KyiChrW2GlgKXHDUoQuAr47ztRjAd9S+OpynICHZi69hlFAFDT9Z9z5M+xaUF0DGUCdkaEp3ERG/CqZJ1R4DXjLGLAHmA3cAmcCzAMaYfwF7rLX315//AXCPMWY5sBDojfOU4z/W2pB8R/Qb/dPxGFi3r5g9ByvokhTtdkltg7VOX4wZDwIW+l4MV72gV1dFRAIgaIKGtfYNY0wH4FdABrAGmGit3Vl/SiZHPsH4LU7/jd8CXYA8nPDxy4AV3co6xEUysnsyi3cc4PP1OdwypofbJYU+nw8++QUs+pvz+Yzb4ZI/qD+GiEiAmPb8hkP9K65FRUVFJCQkuF0OAH+bs5WHP97AuN6pvHzbmW6XE9rqauC9qbD6TcDAhb+FMd/X+BgiIq2guLiYxMREgMT6fo9NCoo+GnLYRac5/WHnbyvQJGunoqYC3rjJCRmeMLjqeRj7A4UMEZEAU9AIMj1SYxmQkUCdz6pT6MmqLIaXr4ZNn0BYFFz3Kgy+2u2qRETaJQWNIHTJIOepxsdr9rlcSQgqL3TeLNk5FyIT4OZ3oe9FblclItJuKWgEoYagMXdLPsWVNS5XE0LKC+Ffl8G+FRDTAb79AXQ/3gj2IiISCAoaQahPx3iy0mKpqbPMXJ/rdjmhoawApl0G+1dDbBrcOh06D3O7KhGRdk9BI0hNHJwBwPTVaj75WmUFzpOMnNUQmw7f/hDS+7tdlYiIoKARtC6ubz6ZsymPsqpal6sJYmUFTp+MnDUQ1xFu/UghQ0QkiChoBKmBGQlkpsRQVetj9sY8t8sJTpVF8PIkyF3rhIxvfwhpfd2uSkREGlHQCFLGGL19ciLVZfDKtbBv5eGOnwoZIiJBR0EjiF1S309j1oZcKmtCcvoW/6itcgbjyl4AkYnOK6xp/dyuSkREmqCgEcSGdk2kc2IUZdV1fLFJzScA1NXCv6fA1pkQHgs3vuXMxCoiIkFJQSOIGWO4qL755JM1+12uJghYCx/8GDZ8CN4IuP5VyNR8MCIiwUxBI8g1vOb633U5VNW28+aTWQ/BipfBeOCaF6HXuS4XJCIiX0dBI8iNzEymU0IUJVW17fvtkyX/hC/+6Gxf+hj0v9TdekREpFkUNIKcx2P45hDnqcYHK/e6XI1LNn4MH93jbJ99H5z+HXfrERGRZlPQCAGXDesMwGfrc9rf4F27l8Bb3wHrg2E3wYQH3K5IRERaQEEjBAzukkj3DjFU1vj4bH07mjr+4C54dTLUVkDv8+FbT4AxblclIiItoKARAowxXDbUearRbppPqkrhteuhPB86DoZrpoE33O2qRESkhRQ0QkRD0JizKY+i8jY+dbzPB+/c7sxfEpsO178GkXFuVyUiIidBQSNE9OkYT/9O8dTUWT5Z28aHJP/817BxOngj4bpXIamb2xWJiMhJUtAIId+qf6rxn7bcfLLiNZj3hLN9+V+h2xnu1iMiIqdEQSOENDSfzN9aQG5JpcvV+MGeZc7InwDjfwZDrnW3HhEROWUKGiGkW0oMw7ol4bMwfVUbaz4pK4A3b4G6Kug3ESb80u2KRESkFShohJhDb5+0paDhq4O3p0BRNqRkwaRnwaN/NUVE2gL9bR5iLh2SgTGwdOcBsgvL3S6ndcz6HWybDeExMPlliEp0uyIREWklChohpmNCFGdlpQLw7vI9LlfTCjZ8BF8+6mxf9hfoONDdekREpFUpaISgK0d0AeCdZbux1rpczSko2Arvfs/ZHj0VBl/tbj0iItLqFDRC0MWDOhET4WVHQTnLdh1wu5yTU1sF/54CVcWQORYu+I3bFYmIiB8oaISgmIgwLhnkzOj69rIQbT757NewbwVEp8BVz2t4cRGRNkpBI0RdVd988uHKvVTW1LlcTQtt/AQWPOVsX/EMJHZxtx4REfEbBY0QNbpXBzonRlFcWcvn63PdLqf5ivbAe3c526OnQr+L3a1HRET8SkEjRHk8hkmNOoWGBF+dM1laRSFkDIXz/5/bFYmIiJ8paISwK0d0BWD2pjzySqpcrqYZvnwMds6DiDi4+p8QFul2RSIi4mcKGiEsKy2OYd2SqPPZ4J9obe9ymPN7Z/vSR6FDlrv1iIhIQChohLirQqH5pKYC3rkTfLUw8HIYMtntikREJEAUNELcN4d0JtxrWLu3mA37i90up2mf/wbyN0JcR/jmE2CM2xWJiEiAKGiEuOTYCM7r3xGANxcH4VONbXNgwdPO9uVPQUyKu/WIiEhAKWi0AZNHdQPgneW7g2tMjYqD8N5UZ3vkd6DPBe7WIyIiAaeg0Qac3SeNzolRHCyvYca6HLfLOezTB6B4NyT3hAt/63Y1IiLiAgWNNsDrMVxzuvNU4/VFu1yupt6Wz2DFK4CBSX+DyDi3KxIRERcoaLQR15zeFWPgq60F7Cwoc7eYqhL44G5ne/RdkHmmu/WIiIhrFDTaiK7JMZzdJw2ANxZnu1vM5/8HRdmQlAnf+B93axEREVcpaLQh153hNJ+8tXQ3tXU+d4rYtQAWPedsf+vPEBHrTh0iIhIUFDTakPMGdCQ1LoK8kipmbnBhorWaSvjPDwELw2+CrAmBr0FERIJKUAUNY8xUY8x2Y0ylMWapMWb815yfZIx5yhizr/47640xEwNVb7CJCPNwVf38J640n3zxCORvcgbm0lsmIiJCEAUNY8xk4Angd8Bw4EvgY2NM5nHOjwD+C/QArgb6AbcDewJRb7CaXN98MmtjLvuKKgJ349wNMO8JZ3vinyA6OXD3FhGRoBU0QQO4B3jBWvu8tXa9tfZuIBu46zjnTwFSgCustfOstTuttXOttSsDVXAw6pUWx6ieKfhsAEcKtRY++qkzl0m/S2HgZYG5r4iIBL2gCBr1TydGAjOOOjQDGHucr10GzAeeMsbkGGPWGGMeMMZ4T3CfSGNMQsMCxLdG/cHmhlHOQ6DXF+8KTKfQVW/CzrkQFg2X/N7/9xMRkZARFEEDSAW8wNHDWuYAnY7znV44TSZeYCLwW+CnwC9PcJ/7gaJGSxBODnLqLhnciQ6xEewrquSz9X4eKbTiIMyo/5Gfc5/zSquIiEi9YAkaDexRn00T+xp4gFzgDmvtUmvt6zj9O47X1ALwMJDYaOl6auUGp8gwL9fVz3/yr/k7/Xuzmb+FsjxI7QtjfuDfe4mISMgJc7uAevlAHcc+vUjn2KccDfYBNdbaxrOIrQc6GWMirLXVR3/BWlsFVDV8Nm14uvIbzuzOM7O38tXWArbkltA73Q+tRHuXw+Lnne1LH4WwiNa/h4iIn1lrqa2tpa4uiCalDAJer5ewsLBT/l0ZFEHDWlttjFkKXAC82+jQBcD7x/naPOAGY4zHWtvQEaEvsK+pkNHedEmK5vwBHZmxLoeX5u/k15cPat0b+Orgw3sAC4OvhZ5nt+71RUQCoLq6mn379lFeXu52KUEpJiaGjIwMIiJO/n8kgyJo1HsMeMkYswSnk+cdQCbwLIAx5l/AHmvt/fXnPwP8EHjSGPMXoA/wAPDnQBcerG4Z04MZ63J4e9ke7r24P3GRrfiPe8WrsHcZRCZozAwRCUk+n4/t27fj9Xrp3LkzERERbfpJd0tYa6muriYvL4/t27fTp08fPJ6T620RNEHDWvuGMaYD8CsgA1gDTLTWNnQyyAR8jc7PNsZcCDwOrMIZP+NJ4A8BLTyIndW7A73SYtmWV8a7y/dw8+jurXPhqhL4/DfO9jk/h/iOrXNdEZEAqq6uxufz0a1bN2JiYtwuJ+hER0cTHh7Ozp07qa6uJioq6qSuE1SdQa21T1tre1hrI621I621XzQ6dq619tajzp9vrR1trY2y1mZZax86qs9Gu2aMORQuXpq/A2uP16+2hb58FMpyISULRt3ROtcUEXHJyf6fenvQGj8b/XTbuKtGdiUmwsumnFIWbi889Qse2AHzn3K2L/qdOoCKiMgJKWi0cQlR4VwxvAsA/5q/49QvOONBqKuGXhOg78Wnfj0REWnTFDTagW+P6QHAJ2v2k114Cj2rd8yF9f8B44GLHgJ1mhIRka+hoNEO9OsUz/g+qfgsTPtqx8ldxFcHn/zC2T59CnQc2Gr1iYhI26Wg0U58d1xPAF5fnE1JZU3LL7DyNdi/GqIS4dwHWrk6ERFpqxQ02olz+qbRJz2O0qpa3lic3bIv11TArIec7bPvhdgOrV+giEgQsNZSXl3rynKybwbOnTuXUaNGERUVRWpqKo8//ngr/1ROTdCMoyH+ZYxhyrie3P/Oav45bwe3ju1BmLeZOXPR36F4DyR0hTNu92+hIiIuqqipY+CvPnXl3ut+cxExES37tTx9+nS+/e1v88gjjzB27FimTZvGPffcw2WXXUZWVpafKm0ZPdFoRyYN70JKbAR7Dlbw6dpmzupaccAZNwNgwgMQfnIDtoiISOuqrKzkzjvv5Mknn+TWW2+lb9++/OY3vyE+Pp45c+YwadIkkpOTufrqq4/57omOtTY90WhHosK93DS6O3/+fDPPz93GpUMyvv5Lc5+AyoOQNgCGXuf/IkVEXBQd7mXdby5y7d4tMXPmTCoqKpg8efKhfQ0ToUVGRvKjH/2IKVOmMG3atGO+e6JjrU1Bo525eXR3np29leW7DrJ05wFGdk8+/snFe2Hhs872+f8Lnpb9RyAiEmqMMS1uvnDLrFmzGDp0KF7v4b+bt2/fzoEDBxgxYgQDBgxg9uzZTX53woQJxz3W2tR00s6kxUdyxfDOALwwd9uJT579MNRWQuYYDc4lIhJkli9fTnX1kZOV/+UvfzkUMoJFaMQ2aVXfHdeLN5fs5pM1+9mRX0aP1NhjT8rbCMtfdrbP/7UG5xIRCTIrVqzA5/Mxbdo0xowZw5tvvskzzzzDvHnz3C7tCHqi0Q716xTPuf3S8Fn42xfHeaox63dgfdDvUsg8M7AFiojICe3atYuCggJefvll/vSnPzF48GDef/99PvnkE0aMGOF2eUdQ0Ginpp7bG4C3l+4mp7jyyIP7V8O69wED3/ifwBcnIiIntGLFClJSUpg4cSKrV6+mqqqKxYsXc84557hd2jEUNNqpUT1TOKNHMtV1Pp7/8qinGrN/76xPu0JDjYuIBKHly5czePDgE55z0UUXcc011zB9+nS6du3K4sWLm3WstbW4j4Yx5ufW2j8YY4YA6621JzGetQSDqef25jsvLuaVhbv4/oTeJMVEwL5VsOFDwMA5v3C7RBERacLy5csZMmTICc/59NPjDzx2omOt7WQ6g86tX/8/YIAxpgZYA6yuXxZba5s5GpS46dx+aQzISGD9vmJe/GoHd5/f9/DTjEFXQXp/dwsUEZEmvffee26X0Gwtbjqx1s6rX19prR0AjAaeAHKB84Hpxpj/a9UqxS+MMUw91xmi9sWvdlCxcyls/MiZBv6cn7tcnYiItAUtDhrGmB/Xr/sZYzzW2nJr7SJr7QvW2ruttSOBia1eqfjFxMEZ9OgQw8HyGnI/+H/OzkFXQ1pfV+sSEZG24WQ6g66pXz8ObDTGLDPGvGSM+bkx5tL6Y6NbpzzxN6/H8L1zshhittI9/wusnmaIiEgrasplW/UAACAASURBVFHQMMb0BLoZY64HfmKt7QOcCzwDHAAuAFAH0dAyaUQX7oty2vu2ZVwKqb1drkhERNqKZncGNcbcDTwKlAK1QJIxZilwu7X2K+Ar/5Qo/haZt5Zxdil11vCLvIt4ubaOyDDNayIiIqeuJU80fgk8DCRbazsAvXHeQPnKGDPOH8VJgMx9HIDPvWexuCSFNxZnu1yQiIi0FS0JGnHAi9ZaH4C1dru19h6c8PGoP4qTACjYCuucZpOqM38EwFOztlBZU+dmVSIi0ka0JGisAsY0sf8N4MSjhkjwmvekM6dJn4u48LzzyUiMIqe4itcX7XK7MhERaQNaEjR+CjxqjJlszBFTeY4BNrduWRIQxXthxavO9vh7iAzz8v0JTkfQp2dv1VMNERE5Zc0OGtbaucCtwCNAjjHmv8aYOcBjwH3+KU/8av5T4KuBzLGQ6byRfO3p3eiSFE1uSRWvLNRTDREROTUter3VWjsd6IMTOJYD1YAFPjLG5BljZhpjnmj1KqX1lRfCkn862+PvObQ7Isxz6KnGM7O3UlGtpxoiIsFs7ty5jBo1iqioKFJTU3n88cfdLukIJzMEeZW1drq19j5r7QXW2jSgBzAFmA10b90SxS8W/g1qyqDTYOh9/hGHrh7ZlS5J0eSXVvHSgh3u1Cci4gZrobrMncXaFpc7ffp0Jk2axNSpU1m1ahV33nkn99xzD1u3bvXDD+fkGHsSf7C2whiTABQVFRWRkJDgdjmBU10Gj58GFQfg6n/CoCuPOeXNxdnc9/YqEqPD+eK+CSRGh7tQqIiI/1RWVrJ9+3Z69uxJVFSUs7O6DB7q7E5BD+yFiNhmn15ZWUmfPn34wx/+wA033ABAXV0dycnJ/PSnP2XWrFnk5uYSFhbGgw8+yDXXXANAdnY2N998c5PHmrrHMT+jesXFxSQmJgIkWmuLj1fnyQxBLqFuxatOyEjuCQMvb/KUK0d0oU96HEUVNfxtTvAkYxERccycOZOKigomT558aJ/X6yUsLIzevXvzxBNPsG7dOj777DN+8pOfUFZWBkBYWNhxj/nDyUwTL6HM54MFTzvbo6eCp+kRQMO8Hu69qB93vLSUf8zbzrfH9qBjQlST54qItBnhMc6TBbfu3QKzZs1i6NCheL2H/x7fvn07Bw4cYMSIEQwYMACA9PR0UlJSKCwsJDY2loyMDDIyMpo85g8KGu3Npk+gcBtEJcKwG0546gUDOzKyezJLdx7gic828/CVgwNUpIiIS4xpUfOFm5YvX051dfUR+/7yl78cETIAlixZgs/no1u3bsdc40THWouaTtqb+U8565Hfgci4E55qjOEXl/QH4M0l2WzNK/V3dSIi0kwrVqxg7dq1TJs2jU2bNvHb3/6WZ555hr///e+HzikoKOCWW27hueeeO+b7JzrWmhQ02pO9y2HnXPCEwag7mvWVM3qkcP6AdOp8lj99utHPBYqISHPs2rWLgoICXn75Zf70pz8xePBg3n//fT755BNGjBgBQFVVFZMmTeL+++9n7NixR3z/RMdam5pO2pP59X0zTrsSErs0+2v3XtSfmRty+XjNfpbvOsDwzGQ/FSgiIs2xYsUKUlJSmDhxIhMnTjzmuLWWW2+9lW984xvcfPPNzT7mD3qi0V4U7YG17zjbY6a26Kv9OsVz1YiuADw8fQPt+ZVoEZFgsHz5cgYPPn6/uXnz5vHGG2/w3nvvMWzYMIYNG8bq1au/9pg/6IlGe7HoOfDVQvdx0Hl4i79+z4V9+WDVXhbtKGT66v1cOiTDD0WKiEhzLF++nCFDjj+f6bhx4/D5fC0+5g96otEeVJXC0vrhxsd8/6QukZEYzZ1nZwHw0PT1mnBNRMRF7733Hn/+85/dLqNZFDTag1VvQGURpPSCvhef9GW+d04WGYlR7DlYwQtzt7digSIi0lYpaLR11sKi+ledRt0BnpP/Rx4d4eXnFzuvuz41awu5xZWtUaGIiLRhChpt3c55kLfeGXFu6PWnfLnLh3VmeGYS5dV1/FGvu4qIyNdQ0GjrGp5mDLkWopNO+XLGGH71zYEA/HvpblbvLjrla4qISNuloNGWFe+DDR8622fc3mqXHZ6ZzKThzjgcv/5grV53FZGQpr/Djq81fjZBFTSMMVONMduNMZXGmKXGmPHN/N51xhhrjHnP3zWGlKUvOq+0Zo6BToNa9dL3XdyP6HAvS3Ye4O1le1r12iIigRAeHg5AeXm5y5UEr4afTcPP6mQEzTgaxpjJwBPAVGAecCfwsTFmoLV21wm+1x34E/BlQAoNFXU1TtAAOOO2Vr98RmI0Pz6/D7//eAMPT1/P+QPSSYqJaPX7iIj4i9frJSkpidzcXABiYmIwxrhcVXCw1lJeXk5ubi5JSUlHzBDbUkETNIB7gBestc/Xf77bGHMRcBdwf1NfMMZ4gVeA/wXGA6feCaGtWP8BlO6H2HQYcJlfbjHlrJ68vXQ3m3NLeeTTjfxukmZ3FZHQ0qlTJ4BDYUOOlJSUdOhndLKCImgYYyKAkcDvjzo0AzjRbC+/AvKstS80p5nFGBMJRDbaFd/SWkPG4vq8NvJWCPPPk4aIMA//d8UgrntuAa8u2sU1p3djWDdlPREJHcYYMjIySE9Pp6amxu1ygkp4ePgpPcloEBRBA0gFvEDOUftzgCajlDHmLOC7wLAW3Od+nKcfbVvOWue1VuOF07/j11uN7tWBK4d34Z3le/if91bz/vfH4fXo0aOIhBav19sqv1TlWEHVGRQ4unuraWIfxph44GXgdmttfguu/zCQ2GjpepJ1Brcl9cON978UEjr7/Xb3TxxAQlQYa/YU88rCnX6/n4iIhI5gCRr5QB3HPr1I59inHABZQA/gA2NMrTGmFrgFuKz+c1ZTN7HWVllrixsWoKTV/gTBorocVr3pbJ8+JSC3TIuP5N76EUMf+XSjRgwVEZFDgiJoWGurgaXABUcdugD4qomvbAAG4zSbNCz/AWbVb2f7rdhgt/4/UFUESd2h5zkBu+0NozIZ2jWRkspafvX+2oDdV0REgltQBI16jwG3GWOmGGMGGGMeBzKBZwGMMf8yxjwMYK2ttNauabwAB4GS+s/Vrv0p3LZ0mrMecfMpzWvSUl6P4fdXDSHMY/hk7X4+Xr0vYPcWEZHgFTRBw1r7BnA3zpskK4CzgYnW2oZG/0wgw6XyQkP+Ztj1FRgPDLsx4LcfkJHA1HOdVqsH31/LwfL2m/dERMQRNEEDwFr7tLW2h7U20lo70lr7RaNj51prbz3Bd2+11l4RkEKD1bL6pxl9LgpIJ9CmfP8bvemdHkd+aRX/9+F6V2oQEZHgEVRBQ05BbTWseM3ZHnGLa2VEhnn5w1VDMAbeXrabOZvyXKtFRETcp6DRVmycDuX5ENcJ+lzoaikjuydz69geADzwzmpKq2pdrUdERNyjoNFWLPuXsx5+I3jdH4ftZxf2o2tyNHsOVvDQdDWhiIi0VwoabcHBXbB1prM9/CZ3a6kXGxnGH68eAsCrC3cxa4PmERARaY8UNNqC5S8D1hk3I6WX29UcMjYrlSln9QTgvrdXUVimt1BERNobBY1Q5/PBiledbRc7gR7PfRf3o3d6HHklVfzPe6ux9pgR5UVEpA1T0Ah1O+dBUTZEJjpzmwSZqHAvT0weRpjHMH31ft5bscftkkREJIAUNELdyvpXWk+7AsKj3a3lOAZ1SeTH5/UB4Ffvr2XvwQqXKxIRkUBR0Ahl1WWw7n1ne+j17tbyNe46N4vhmUmUVNZy9xsrqPOpCUVEpD1Q0AhlGz6C6lJI7gGZo92u5oTCvB4ev3YYsRFeFm0v5C8zN7tdkoiIBICCRihr6AQ69Howxt1amqFHaiy/mzQYgD9/vpkF2wpcrkhERPxNQSNUFe+FbbOd7SGTXS2lJa4Y3oWrR3bFZ+Hu11folVcRkTZOQSNUrXoTsJA5FlJ6ul1Ni/zm8tPolRbL/uJK7n1rpV55FRFpwxQ0QpG1h982GXqdu7WchJiIMJ66YQQRYR4+35DLP+btcLskERHxEwWNULRvBeRtgLAo57XWEDQgI4EHvzkQgIenr2fJjkKXKxIREX9Q0AhFK1931v0vhahEd2s5BTedmcm3hnam1meZ+soycksq3S5JRERamYJGqKmrgdVvOdtBPnbG1zHG8PsrB9O3Yxy5JVX84JXl1NT53C5LRERakYJGqNk2B8oLIDYNek1wu5pTFhsZxrM3jSQ+MoxFOwo1pbyISBujoBFq1vzbWQ+8Arxh7tbSSnqlxfHotUMB+Oe8Hbyv+VBERNoMBY1QUlMJ6z90tgdd5W4trezC0zrx/QlZAPz87VWs2VPkckUiItIaFDRCyeYZUF0CCV2h25luV9Pq7rmgH2f3TaOyxsdt05aQW6zOoSIioU5BI5SsedtZD5oEnrb3j87rMfzl+uFk1Q/mdftLS6msqXO7LBEROQVt77dVW1VVAps+cbYHXe1uLX6UGB3OC98+g6SYcFZmH+Tef6/SyKEiIiFMQSNUbJgOtZXQoTdkDHW7Gr/qkRrL0zeOIMxj+GDlXv46c4vbJYmIyElS0AgVh5pNrgqJmVpP1disVH5z+SAAHv3vJr2JIiISohQ0QkF5IWz93NluY2+bnMgNZ2Yy5SxnwrifvbWSr7bmu1yRiIi0lIJGKFj/H/DVQsfBkNbP7WoC6peXDmDi4E7U1Fnu/NdSNuwvdrskERFpAQWNULC6fpCuwe3naUYDr8fw2LXDGNUjhZKqWm79x2L2HqxwuywREWkmBY1gV7Ifdsx1tk+70t1aXBIV7uW5W0bSOz2O/cWV3PrPRRSV17hdloiINIOCRrBb/wFgocvpkNzd7WpckxQTwbQpo+iYEMmmnFKmTFtMeXWt22WJiMjXUNAIduved9YDL3e3jiDQJSmaF78zioSoMJbuPMDt/1qiAb1ERIKcgkYwK8uHnfOc7YGXuVtLkBiQkcCLU0YRE+Fl3pYCfviappYXEQlmChrBbMOHYH3OAF3JPdyuJmiMyEzm+VtOJyLMw3/X5fCzt1bi82n0UBGRYKSgEczW/cdZD9DTjKON7Z3KM/Wjh76/Yi8PvLtaYUNEJAgpaASrigOwfY6zrf4ZTTpvQEcenzwMj4HXF2crbIiIBCEFjWC18RNnkK60AZDax+1qgta3hnbmsWsPh41fvLNKYUNEJIgoaAQrvW3SbFcM73LoycabS3Zz39urqFPYEBEJCgoawaiqBLbOdLb1tkmzXD6sC09eNxyvx/Dvpbu5998rqdXbKCIirlPQCEabPoW6KkjJgvSBblcTMr41tDNPXjcMr8fwzrI9/ODV5VTVapwNERE3KWgEo/X1b5sMvKxdTAnfmr45pDNP3ziCCK+HT9buZ8qLiymr0giiIiJuUdAINtXlsPm/zrb6Z5yUi07rxIvfOePQoF43Pr+Qg+XVbpclItIuKWgEm62fQ005JGVCxjC3qwlZY3un8urto0mKCWdF9kGu/dt8zfoqIuICBY1gs+EjZ93/m2o2OUXDuiXx5p1jDk3ENunpeazdW+R2WSIi7YqCRjCpq3U6ggL0m+huLW1E347xvH3XWPqkx5FTXMW1z85n1sZct8sSEWk3gipoGGOmGmO2G2MqjTFLjTHjT3Du7caYL40xB+qXz4wxowJZb6vbvQgqCiEqCTLHuF1Nm9E1OYZ/3zWWsVkdKKuu47ZpS3h14S63yxIRaReCJmgYYyYDTwC/A4YDXwIfG2Myj/OVc4HXgAnAGGAXMMMY08X/1frJxunOuu9F4A1zt5Y2JjE6nBe/M4orR3Shzmd54N3V/P7jDRpFVETEz4ImaAD3AC9Ya5+31q631t4NZAN3NXWytfZGa+3T1toV1toNwO04f57zAldyK9v4sbPud4m7dbRREWEeHr1mKHef7wzp/uycrXzv5aWU6vVXERG/CYqgYYyJAEYCM446NAMY28zLxADhQOEJ7hNpjEloWID4k6nXL/I3Q8EW8IRDVuhmpWBnjOHu8/vy6DVDifB6mLEuh0lPzWN7fpnbpYmItElBETSAVMAL5By1Pwfo1Mxr/B7YA3x2gnPuB4oaLbtbVqYfNTSb9BwPUQnu1tIOXDWyK6/fOZqOCZFszi3lsr/OVSdRERE/CJag0eDoBnPTxL5jGGPuA64HrrTWVp7g1IeBxEZL15Oss/UdajbR2yaBMiIzmQ9+MI6R3ZMpqaxlyouLeWrWFqxVvw0RkdYSLEEjH6jj2KcX6Rz7lOMIxpifAQ8AF1prV53oXGttlbW2uGEBSk6h5tZTlg/ZC51t9c8IqPSEKF67fTQ3nJmJtfDIpxv53stLKSqvcbs0EZE2ISiChrW2GlgKXHDUoQuAr473PWPMvcCDwMXW2iX+q9DPNs8A64NOQyAxeB6ytBcRYR4emjSYhyYNJtxr+HRtDpf+5UuW7zrgdmkiIiEvKIJGvceA24wxU4wxA4wxjwOZwLMAxph/GWMebji5vrnkt8AUYIcxplP9EudG8aekYTRQNZu46oYzM3n7rrFkpsSw+0AF1zw7n79/sU1NKSIipyBogoa19g3gbuBXwArgbGCitXZn/SmZQEajr0wFIoB/A/saLT8LVM2toqYSts50ttVs4rohXZP48EfjuHRwBrU+y++mr+e2aUs4UKZJ2UREToZpz/+3Vv+Ka1FRUREJCS696bFpBrx6DSR0gZ+s1fwmQcJayysLd/GbD9dRXeujY0Ikj1w9lLP7prldmohIUCguLiYxMREgsb7fY5OC5olGu9XwWmu/SxQygogxhptGd+e9qWfRKy2WnOIqbvnHIh58bw3l1RrgS0SkuRQ03GQtbP6vs93nIndrkSYN7JzARz8cz7fHdAfgpQU7mfjklyzdedxx4UREpBEFDTflbYDi3RAW5QzUJUEpOsLLry8fxMvfPZOMxCh2FJRzzbPz+cMnG6isqXO7PBGRoKag4abN9SOu9xgP4dHu1iJfa1yfVD65+2yuHN4Fn4VnZm9l4p+/ZOG2ArdLExEJWgoabjrUbHL08CESrBKjw3ls8jCevWkEafGRbMsrY/JzC/jF26s0yJeISBMUNNxSWQy7Fjjbvc93txZpsYsHZfDZT87h+lGZALy+OJvzHpvDR6v2adwNEZFGFDTcsn0O+GogJQs6ZLldjZyExJhwHr5yMG/cMZpeabHkl1bx/VeX8e1/LmZrXqnb5YmIBAUFDbeo2aTNOLNXBz7+8Xh+fF4fIrwevtiUx0WPf8FD09dTUqnmFBFp3xQ03HDEa60KGm1BZJiXn1zQl09/cjbn9U+n1md57ottTPjTHP69dDc+n5pTRKR9UtBwQ+46KNkLYdHQfZzb1Ugr6pkaywu3nsE/bz2DnqlOc8rP3lrJpGe+YoHeThGRdkhBww0Nr7X2HA/hUe7WIn4xoX86n959Nr+4pD+xEV5WZh/kuucWMOXFxWzYf9yRekVE2hwFDTds/sxZ97nQ3TrEryLCPHzvnCxm3XsuN43OxOsxzNyQyyVPfsnP3lrJ3oMVbpcoIuJ3mlQt0JOqVRbBH3uBrxZ+tAJSegbmvuK6bXml/GnGRqav3g84QeSGUZncdW4WHRP0ZEtEQosmVQtW22Y7IaNDH4WMdqZXWhxP3ziSd6eOZVTPFKprfbz41Q7G/3EW/+8/a8kprnS7RBGRVqegEWh626TdG56ZzBt3jOaV287k9O7JhwLH2fWBQ00qItKWqOkkkE0n1sJjA503Tm56B3qf5/97SlCz1vLV1gIe/+8mluw8AECYx3DZsM7ceXYW/TrFu1yhiEjTmtt0oqARyKCRuwGePtOZrfXnO/XGiRxirWXelgL+OmszC7YdnoJ+Qr807jg7i9G9UjDGuFihiMiRmhs0wgJXkrBtlrPOHKOQIUcwxjCuTyrj+qSyMvsgz32xjY/X7GPWxjxmbcxjaNdEvju+Fxef1omIMLV4ikjoUNAIpK31QSNrgrt1SFAb2i2Jp24cwY78Mp6fu423luxm5e4ifvTactLiI7l+VCY3jMqkU6LCqogEPzWdBKrppLYa/tADasrgzi8gY6h/7ydtRkFpFS8t2MmrC3eRW1IFgNdjuOi0jtw8uoeaVUTEFeqj0QwBDRo75sGLEyEmFX62GTx6/C0tU1Pn49O1+/nX/J0s2n64H0fv9Dgmn96NK4Z3IS0+0sUKRaQ9UdBohoAGjZm/gy/+CIOugqv/4d97SZu3YX8xL83fybvL91BeXQc4b6tM6J/ONSO7MqF/OuFehVkR8R8FjWYIaNB4/nzYvRgu+yuMuNm/95J2o7iyhg9X7uPNJdmsyD54aH9qXASThnfh8mFdOK1zgppWRKTVKWg0Q8CCRsVB+GNPsD74yVpI7Oq/e0m7tTmnhLeW7uadZXvIL606tD8rLZZvDe3MZUM70ystzsUKRaQtUdBohoAFjfUfwBs3OcOO/3CJ/+4jgtOXY87GPN5etpvPN+RSXes7dOy0zglcNrQzEwdn0C0lxsUqRSTUaRyNYKLXWiWAwr0ezh/YkfMHdqSksoYZa3P4YNVevtycz9q9xazdW8zDH29gYEYCF57WkYtO60T/TvFqXhERv9ATjUA80fjzcCjcBte9Bv0n+u8+IidQWFbN9NX7+GDlXhbvKMTX6D/9binRXDiwExcO7MjpPVLwehQ6ROTE1HTSDAEJGgd2wpNDwHjh5zsgKkDT0YucQGFZNZ+tz2HG2hy+3JxHVaPmleSYcMb3SeOcvmmM75tKerwGBhORYyloNENAgsbSF+GDH0O30fDdT/1zD5FTUF5dyxeb8pmxbj+fr8+lqKLmiOOndU7gnL5pnN03jZHdk/XarIgA6qMRPNQ/Q4JcTEQYFw/qxMWDOlFT52NF9kHmbMxjzqY8Vu8pOtSv4+nZW4mLDGNUzxRG90phTK9UBnZOUDOLiJyQnmj484mGrw4eyYKKAzDlU8gc3fr3EPGj/NIqvtycxxeb8vliUx4FZdVHHI+PCmNUjxTGZHVgdK8ODMhQ8BBpL9R00gx+Dxr7VsLfzoaIePj5dvCGt/49RALE57Os21fMgm0FLNhWwMLthZRU1h5xTlxkGMO6JTGiezIjMpMY3i2ZxBj9ey/SFqnpJBhs/9JZdx+jkCEhz+MxDOqSyKAuidw2vhd1Psu6vU7wmL+tgEXbCymtqmXulnzmbsk/9L3e6XGMyExiRGYyI7onk5UWp6ceIu2IgoY/7agPGj3Gu1uHiB94PYbBXRMZ3DWR2892gsfG/SUs3XWA5TsPsGzXAXYUlLMlt5QtuaW8uWQ3ADERXgZmJBwKLYO6JNA7LY4wdTIVaZPUdOKvppO6WmfY8apiuGM2dB7eutcXCQEFpVUs33WQpbsOsGznAVbtLqKipu6Y8yLDPAzISGBQlwQGZiTSr1McfTrGkxClJ4EiwUp9NJrBr0FjzzL4+wSITHT6Z3i8rXt9kRBU57Nszy9l9Z4i1uwpZk39Wy2lVbVNnt85MYq+neLp1ymefh3j6dsxnt7pcUSF678nEbepj4bbGppNuo9VyBCp5/UYeqfH0zs9nkn1D/l8PsvOwnLW7ClizZ4iNuwvYVNOCfuKKtlbv8zemHfoGh4D3TvE0is1ll5psfRMjaNXmvM5LT5SQ6mLBBkFDX9p6AjaU/0zRE7E4zH0TI2lZ6ozy2yDoooaNuWUsLE+eGzcX8LGnBIOltewPb+M7fllfL7hyGvFRYYdulbP1Fh6pMbQLTmGbikxpMVF4lEnVJGAU9Dwh7pa2DXf2VZHUJGTkhgdzhk9UjijR8qhfdZa8kqr2JxTyrb8MrbllR4KHdmF5ZRW1bJ6TxGr9xQdc72IMA9dk6Prg0f0oQDS8DkxOlxPQ0T8QEHDH/atgOpSiEqCjoPcrkakzTDGkB4fRXp8FGf1Tj3iWFVtHdmF5WzLc4LHtrwydhaWkV1Ywb6iCqprfWzLc/Y3JTrcS0ZiFJ3qF2c7moyEw59TYiMURkRaSEHDH7Z/4ax7jAOPXtkTCYTIMO+h/h9Hq6nzse9gJdkHyskuLK9fVxxa55dWUVFT5zwlyW86iIDzVKRTQhSdEqJIjY8gNS6StLhIUuMjne34SFLjnP3qsCriUNDwB42fIRJUwr0eMjvEkNkhpsnjlTV17C+qZF9RJfuLK5x1w+f6dX5pFdW1PnYVlrOrsPxr7xkfFXYohKTFRZISG0FyTDhJMRGkxEaQFBNOckyEs8SGExcZpqcl0iYpaLS2uhrYtcDZVkdQkZAQFe6lR2osPVJjj3tOda2PnOJK9hdXklNcSX5JFXmlVeSXVJNf2rBdRX5pNdV1PkoqaymprD3hE5LGwjyGpBgnjDSEj+SYCBKiw4mPDHPWUWHER4WT0LCOdtbxkWHq6CpBS0Gjte1ZBjXlEJ0CaQPcrkZEWklEmMfpPJrS9FORBtZaiitqneBRv+SVVHGgrJoD5TUcKK92lrIaDpZXU1heTWWNj1qfPXR+SxkDcRFhxEcdDiQJUc46JjKM2AgvMRFhxEYetY4IIybS66wjvMRGOuvIMI+erkirCaqgYYyZCtwLZABrgbuttV+e4PyrgP8DsoCtwC+tte8Gotbj2qH+GSLtmTGGxJhwEmPC6Z0e16zvVNbUHQofh4JIeQ0Hy6oprqyhpLK20bqWkooaiuv3Vdf6sBZKqmopqaplb1HlKf8ZvB7jBI/6IBIT4SUqzEtUuJeocA+R4Q2fPYf2RYc7x51jDfsbndPo/MhwD5FeLxFhHsK9RsPPt3FBEzSMMZOBJ4CpwDzgTuBjY8xAa+2uJs4fA7wBPAi8C0wC3jTGjLPWLgxc5Uc5NH7G2a6VICKhJSrcS0ZiNBmJ0S3+bmVNXX0zjRM+ShoCSYWzLq+uo7y6lrLqWsqr6px1dR1lVf+/vXsPkqss8zj+/fWEpCSyWyCkQDCAKyAi14gKAmGFsCXlctEVWAVXvw5O7wAADlxJREFUi5I1WZbLWkgFXC6CEnA3RC4WKCoX5aa7lV0UBNkl3BdIQMANSBa5h4CAQCSZzEz3s3+8byedTs9MJ+mTMz35faq6zvR73vf0c04mc55+z/uek5cN5b39NSDdwbV+6WddqCiNoxk7psLY+nJMJZX1VNhgTIVxPZXlicnydY31c72xPalOT6W+TInMmIrSq0eMqeT39fJcb4OeSlpW8rKpvN52pXWVii9bDWPE3IJc0gPAwxExtaHsCWB2RExvUf8G4M8i4lMNZb8C/hgRf9vmZ3b2FuQDy2DG1jCwFKb9D0zwpRMz6x7VWrCkORHJy97+Kr0DVXr7a+nn+nKgyrLlZbl8oLpynRblo4kEPRIViUol/1xJiUgll/fkcimVp3U01UntKmrYRi5XrttYXqmwvN2KbZPrp+3Uty3BX+4wgf2236xj+91VtyCXNBaYBMxoWnUbsPcgzfYCLmwquxU4aYjPGQeMayhadR7c2nhpXkoyxm8Gm32wo5s2MytaT0VpcGnBD7OLCAZqQd9Ajf5qjb6BGn152V+N/L5K30DQV63Rn9f3V2ssG6it1K6+XFat0T8Q9FWrVGtBfzXystbwPo2FGagGAyv9HAzU69VqVKtBf23l9vU2tRbfzSNgIAIIWPWZgSPGJhuO7Wii0a4RkWgAmwI9wCtN5a8Amw/SZvPVrA8wHThzTQJsy+tPQ2WDND7DA6nMzFqS0qWHDbpwbEatlhOTpqQlIvUIVWtBLepLlv9cL6/leivqBLUaVCOorVQvqObyaNpGtUZDnRbbrgUBy8vq7Sdts3Epx2ykJBp1zbmiWpStTf3zgJkN7zcCXmw7uuHscQx8+DPQu+rtj83MrPtVKmJsRYyl+5KksoyUROM1UodTc2/EBFbttahbtJr1iYhlwPK5Y4VM3xo7Pr3MzMxsZKRkEdEHzAOmNK2aAtw3SLP7W9Q/aIj6ZmZmto6NlB4NSJc0rpE0l5REHAdMBC4DkHQ18FLDDJTvAndJOhX4D+BQ4EBgn3UduJmZmbU2YhKNiLhB0nuAM0g37PotcHBEPJerTARqDfXvk3QUcC7ppl1PA0eWeg8NMzMzW8mIuY9GGTp+Hw0zM7P1RLv30RgRYzTMzMxsdHKiYWZmZoVxomFmZmaFcaJhZmZmhXGiYWZmZoVxomFmZmaFGTH30SjT228POivHzMzMWmj33Lm+30djSzr5UDUzM7P1z1YR8dJgK9f3REPAe4HFHdxs/YmwW3V4u+srH8/O8zHtLB/PzvMx7awij+dGwMIYIplYry+d5AMzaBa2JhqeCLt4qDulWXt8PDvPx7SzfDw7z8e0swo+nsNuz4NBzczMrDBONMzMzKwwTjQ6bxlwdl7a2vPx7Dwf087y8ew8H9POKvV4rteDQc3MzKxY7tEwMzOzwjjRMDMzs8I40TAzM7PCONEwMzOzwjjR6CBJ0yQ9I6lX0jxJ+5YdU7eSNF3SQ5IWS3pV0mxJO5Qd12iRj29ImlV2LN1M0paSfiLpdUlLJP1G0qSy4+pGksZIOjf/DV0q6feSzpDk81SbJO0n6SZJC/P/78Oa1kvSWXn9UklzJO1UdFz+B+wQSUcCs4BvAbsDdwO3SJpYamDdazJwKfBxYArpLra3SRpfalSjgKQ9geOAx8qOpZtJ2hi4F+gHPgV8CPga8GaZcXWxU4GvAscDOwJfB04B/rHMoLrMeOBR0jFs5evAP+X1ewKLgF9L2qjIoDy9tUMkPQA8HBFTG8qeAGZHxPTyIhsdJG0GvApMjoi7yo6nW0l6N/AwMA34BvCbiDip3Ki6k6QZwCciwj2XHSDpF8ArEXFsQ9m/AUsi4pjyIutOkgI4PCJm5/cCFgKzIuL8XDYOeAU4NSIuLyoW92h0gKSxwCTgtqZVtwF7r/uIRqU/z8s3So2i+10K/DIibi87kFHgEGCupJ/ly3uPSPpK2UF1sXuAAyRtDyBpV2Af4OZSoxo9tgU2p+E8FRHLgDsp+Dy1Xj9UrYM2BXpImWGjV0j/sLYWciY+E7gnIn5bdjzdStJRwB6kLlNbe+8HppJ+N78NfBS4SNKyiLi61Mi60/mkLxRPSqqS/qaeHhHXlRvWqFE/F7U6T21d5Ac70eis5utQalFmq+8SYBfStxtbA5LeB3wXOCgiesuOZ5SoAHMj4rT8/pE8sG4q4ERj9R0JHA18HvhfYDdglqSFEXFVqZGNLuv8POVEozNeA6qs2nsxgVWzR1sNki4mdVHvFxEvlh1PF5tE+n2c1/DI6B5gP0nHA+MiolpWcF3qZWB+U9kTwGdLiGU0+A4wIyKuz+8fl7Q1MB1worH2FuXl5qTf3brCz1Meo9EBEdEHzCPNjmg0Bbhv3UfU/fI0rEuAzwCfjIhnyo6py/0XsDPpW2L9NRf4KbCbk4w1ci/QPOV6e+C5EmIZDTYEak1lVXye6pRnSMnG8vNUHl84mYLPU+7R6JyZwDWS5gL3k6YPTgQuKzWq7nUpqQv1UGCxpHpv0VsRsbS8sLpTRCwGVhrfIukd4HWPe1ljFwL3SToNuJE0RuO4/LLVdxNwuqTnSZdOdidNxfxRqVF1kTyr7AMNRdtK2g14IyKez/fNOU3SAmABcBqwBLi20Lg8vbVzJE0jzVPegvRH/WRPxVwzeWpWK1+OiCvXZSyjlaQ5eHrrWpH0aeA8YDvSN8aZEfGDcqPqTvleDucAh5O68xcC1wHfzL3GNgxJ+wN3tFh1VUR8KQ+sPxP4e2Bj4AHgH4r+suFEw8zMzArja19mZmZWGCcaZmZmVhgnGmZmZlYYJxpmZmZWGCcaZmZmVhgnGmZmZlYYJxpmZmZWGCcaZmZmVhgnGmajhKQdJC3Kd1gs4/O3kRT5lsfrtXwcDluDdhMk/UHSlkXEZVYGJxpmI5ikOfn5BO34FnBpfq5JvX2PpJMlPSapV9Kbkm6R9InVjCOGeV0JvMCK2+83Jh711x8l3SVp8hCfs7+kZ1cntm4n6dl862gi4lXgGuDsUoMy6yAnGmajgKStgEOAHzeUCbgeOAO4CNiR9KTGF4A5q/mNe4uG10nA201lJ0ZENSIWRcRAU9sDc53Jud3NkrZd7Z1cf/wY+IKkjcsOxKwTnGiYjVC5l2AycGJDr8A2g1Q/Ang0Il5sKvsb4IsRcUVEPBMRj0bEccB/AldIGl//LEmzmz5/Vn7wGjmBWBQRi4C3UtGKsoh4a4hLJ6/nOo+RHua0IXBQm8dgV0l3SFos6W1J8yR9pGH93rmXZKmkFyRdVN+nvH6cpAvyumWSFkg6tmH9ZEkP5nUvS5ohaUzD+jl5mxdIeiNfmjqrKcbtcgy9kuZLmtK0fqykS/L2e3MPxvTB9jkiHic9zvvwdo6R2UjnRMNs5DoRuB/4ASt6Dl4YpO5+wNymss8DT0XETS3q/yvwHmBKi3VFWpKXG7RZ/6fAi8CewCRgBtAPIGln4Fbg34FdgCOBfYBLGtpfDRwFnEDq0fkq8KfcfkvgZuAhYFdgKnAs8I2mGP4OeAf4GOnpzGfUkwlJlfz5VeDjefvnN7U/gdTbdASwA3A08Oww+/0gsO8wdcy6wpjhq5hZGXIvQR+wJPckDGUbYF5T2fbAE4PUf6KhzjqRexrOI52U72xVJyLmkPalbiLwnYh4Mr9f0LDuFODaiKiPYVkg6QTgTklTc9sjgCkRcXuu8/uG9tNIidvxkR5j/aSk9wLnS/pmRNRyvcci4uyGzzgeOAD4Nemy0I7ANvXeJEmnAbc07cMC4J78Oc817XPj/ta9BOzeotys6zjRMBsd3gX0rkG7vk4H0sJ9kmqkSyYvA1/KlwfaMZN0iecY4HbgZxHxdF43CfiApC801Bepp3ZbYGeGSGpICcL9+eRfdy/wbmAr4Plc9lhTu5eBCQ3beL7pktX9TfWvJCUlv5P0K+AXEXHbIDHVLSUdL7Ou50snZqPDa0Dz4MEFwIcGqb9jXj6VlzXSSbpRu5c3hnMk6dLEZhGxZUT8pN2GEXEWsBPwS+CTwHxJ9bELFeByYLeG167AdsDTpJP1UAREizKayvubw2LF387mY9bcloh4mJT4/DMpIbxR0s+HiW0T4A/D1DHrCk40zEa2PqCnjXqPsGpScR2wnaS/blH/a8BC0jdtSCe1LZrqdOp+GC9ExNMR8fqaNI6IpyLiwog4iDQe4st51cPAThHxfy1efcDjpL9xg02nnQ/snWfn1O0NLCZdumjHfGBivuRSt1eLfXg7Im6IiK+QEq/PStpkiO1+mPRvatb1nGiYjWzPAh/LMzo2zYMPW7kV2EtSY1JyPTAbuErSsXkbu0i6HPg0cHRE1L+t/zfwEUlfzLMoziad7Eoj6V15tsb+krbO9/7YkxXjS84n7fOlknbLcR8i6WKAiHgWuAr4kaTDJG2bt3VEbv894H3AxZI+KOlQ0v0rZjaMzxjO7cDvgKvzDJl9SfczadyPkyUdlT9je+BzpFklbw6y3xuSLgsNd3nFrCs40TAb2f6FNM5gPqnXYeIg9W4mdfEfWC/IYw8+B3wbOJl0QnyUNOV194i4o6HurcA5wAWkWRgbkWZslKlKmhlzNekSz42kQZZnAuTpspNJl0ruJvUAnEMaQ1E3Ffg5Kal4kjSDZ3xu/xJwMPBR0nG5DPghcG67AeaE5HBgHGmmyBXA6U3V/gScSpoV9BBpsOvBQyQzh5LGfdzdbhxmI5lWHgdlZt1K0jTg0Ij4qyHq7EH6Fv7DiDhlnQVnbZP0IDArIq4tOxazTnCPhtno8X3gLg3xrJM8MPEA4B1Jf7HOIrO2SJpA6oG5ruxYzDrFPRpmZmZWGPdomJmZWWGcaJiZmVlhnGiYmZlZYZxomJmZWWGcaJiZmVlhnGiYmZlZYZxomJmZWWGcaJiZmVlhnGiYmZlZYf4fjrkbhOqgELUAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 600x400 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.plot(t,np.real(expect(result.states, projection(2,0,0))), label=r'$\\rho_{11}$');\n",
    "plt.plot(t,expect(result.states, projection(2,1,1)), label=r'$\\rho_{22}$');\n",
    "plt.ylabel(r\"$\\rho_{ij}$\")\n",
    "plt.xlabel(\"t (QuTiP 'seconds')\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = mesolve(H, psi0, t, c_ops)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
