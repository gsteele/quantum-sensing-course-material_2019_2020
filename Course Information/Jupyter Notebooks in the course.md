---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Jupyter Notebooks in the course

In the course, we will frequently use <a href=https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html>Jupyter Notebooks</a> to illustrate physics using python. 

The homeworks in week 1-3 will be distributed in Notebooks. The questions will not involve coding, but they may include (interactive) code to illustrate concepts from the course. 

In case you do not yet have Jupyter Notebooks running on your computer, we will also post HTML versions of the homeworks so that you can read and work on the exercises (the demos will not work in HTML). 

In the fourth week, you will work on a Final project in which you will implement simulations of physics problems related to the course in python using the software package <a href=http://qutip.org/index.html>QuTiP</a>. You will use a Jupyter notebook to write a report that includes a definition of the physics of the problem you are working on, your simulation code, visualisation and analysis of the results, and a discussion of the interpretation of your results. 

# Installing Jupyter on  your laptop

We recommend installing python on your computer using Anaconda:

https://www.anaconda.com

You can start the notebook server using the "Ananconda Navigator", although I prefer to use the command `jupyter  notebook` from the command prompt ("Anaconda Prompt" on windows) as this makes things easier to stop and start. 

# Installing QuTiP

You can find the official installation instructions here:

http://qutip.org/docs/latest/installation.html

To install qutip, the easiest way is to open a terminal from the Jupyter server: 

<img src=new_terminal.png>

and then type the following commands: first,

`conda config --add channels conda-forge`

and then: 

`conda install qutip`

