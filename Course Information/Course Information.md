---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

<!-- #region -->
# Course Information TN3155 - Quantum Sensing and Measurement

## About the course

In the course, we will explore concepts in quantum sensing and measurement. What does this mean? In particular, we will explore the concepts of detecting small signals and the consequences that quantum mechanics on our ability to sense and measure quantities. In particular, we will aim to explore and obtain a deeper understanding of quantum noise, and the influence that performing a measurement in quantum mechanics has on the objects we are trying to observe.

This is a very new topic: there is no undergraduate textbook on "quantum sensing and measurement".  We will work primarily from lecture notes developed for the course, with links to more advanced references and reading material, such as research articles, review articles, and advanced textbooks. 

Course schedule:

|  Day / Time | Activity   |
|:---|:---|
| Friday 10.45 - 12.30 | First lecture of "week"  |   
| Monday 10.45 - 12.30 | Second lecture of "week" | 
| Wednesday 10.45 - 12.30 | Third lecture of "week" |
| Thursday 08.45 - 10.30 | Optional Question & Answer session | 
| Friday 09.00 - 10.00 | Weekly test |

Note that the first lecture of a new "week" is scheduled on Friday after the weekly test of the previous "week".

The location of the lectures moves around a lot: different rooms in Building 22, but also sometimes scheduled in the TNW Zuid building. You can check the <a href=https://mytimetable.tudelft.nl>my timetable</a> website for information about room locations for a given day: 

https://mytimetable.tudelft.nl/m/#timetable/2019!module!0AAE29C5E5BCA948A75257A5D7C31AFA

Note that the course has undergone heavy development since the formulation of the description for the Study Guide: we will not be able to cover all the topics in the study guide. Please take the course outline describe below as your expectation for what will be covered in the course.


## Homework exercises

For each lecture, there are a set of homework exercises for you to practice the skills taught in the lecture and to explore yourself the concepts of the course in more depth. The solutions to the homework will be posted the evening before the following lectures. 

The homework will not be graded, but you are welcome to come to the optional question and answer session if you have questions about the material.

## Assessment

The course will be assessed using 3 tests and one final project. 

The three tests will be every week on Friday before the lecture. The test are one hour long, and will cover the material of the previous week. 

In the fourth week, you will work in teams of 2-3 students on a final project in the course, implementing a simulation of a quantum sensing or measurement problem using the quantum simulation software package QuTiP. You will implement this simulation in a Jupyter notebook as a report  which includes an introduction to the background of the topic you are working on, a definition of the problem you will simulate, the code to simulate it, plots and analysis of the results, and a discussion of the physical interpretation of your results. 

The final grade in the course will be determined by:

* **Week 1 Test: 25%**
* **Week 2 Test: 25%**
* **Week 3 Test: 25%**
* **Final project: 25%**

In December, a opportunity during the scheduled "retake exam" will be offered in which you will have an opportunity to retake the Week 1, 2, and 3 tests. This retake moment can count for up to 75% of your final grade. 

## Weekly tests

The weekly test are one hour long and will test your knowledge of the material from during the week. They are "closed book", no formula sheets or material is allowed or needed. We will provide any detailed formulas you will need for the tests. 

What will be on the tests? What will I need to know? Our goal is to make the tests representative of the material from the homework exercises. Our philosophy is: if you solve and understand the homework exercise, you should be fine for the tests.

## Final project

In the last part of the course, you will implement a project simulating and interpreting a problem in quantum measurement and sensing.

In this project, you will work in teams of 2-3 students. **By the end of the first week, we will ask you to inform us of who is in your group.** We strongly encourage (and may enforce) that students from different faculties mix to provide complementary expertise in your project teams. 

We will provide some stand projects that you can work on, but we also encourage you to come up with ideas yourself for things you would like to understand in quantum mechanics that incorporate quantum measurement. **If you have ideas already, please come and talk with us in the first week (or at least before the start of the fourth week) to discuss your idea so we can give you some guidance on how to formulate the problem you will solve.** 

##  Course Outline  Summary

Summary: 

* Week 1: Classical Noise
  * Lecture 1: Noise, measurements, and power spectral density
  * Lecture 2: Where does noise come from? Sources and distributions
  * Lecture 3: Thermomechanical noise of a classical harmonic oscillator
* Week 2: Noise in Quantum Mechanics
  * Lecture 4: Quantum states and their noise
  * Lecture 5: Wigner function representation of the wave function
  * Lecture 6: Collapse noise: what happens if we measure again
* Week 3: How to calculate what happens when we (or someone else) measures
  * Lecture 7: The density matrix
  * Lecture 8: The (Lindblad) Master Equation
  * Lecture 9: Introduction to QuTiP
* Week 4: Project

  
## Course Outline in Detail

###  Week 1: Classical Noise

#### Lecture 1: Noise, measurements, and power spectral density

* Noise, precision, and statistical error in measurements
* What is power spectral density and how do I use it? 
  * Physical interpretation
  * Mathematical definition
  * Difference between PSD and the Fourier transform (subtle)

#### Lecture 2: Where does noise come from? Sources and distributions

* Why Gaussian noise? => Central Limit Theorem
* Examples of central limit theorem:
  * Poisson noise: Shot noise of a laser
  * 1D Random walk: Brownian motion
* Noise in sensors:
  * Reponsitivity  vs. Sensitivity
  * Calculating the sensitivity of a sensor
  * Example: Laser interferometer
  
#### Lecture 3: Thermomechanical noise of a classical harmonic oscillator

* Review of the damped harmonic oscillator:
  * Response of a harmonic oscillator to a sinusoidal driving force
  * Quadrature representation
* Determining the thermal force noise on an oscillator
* Calculting the thermomechanical noise spectrum of the oscillator position
* Thermomechanical noise in the quadrature plane:
  * Gaussianly distribution in the quadrature plane for any given frequency
* Measuring thermomechanical noise with a sensor:
  * Sensor output noise, responsivity, sensor imprecision noise

### Week 2: Noise in Quantum Mechanics

#### Lecture  4: Quantum states and their noise

* Zero point fluctuations of the ground state of the harmonic oscillator (HO)
  * Review of quantum fluctuations in the  HO ground state
  * What do quantum fluctuations mean? 
  * Do quantum fluctuations fluctuate in time? (subtle, depends on if you measure them or not?)
* Quantum noise in another interesting state: the "coherent" state
  * What is the coherent state?
     * Formula
  * What are it's properties?
     * "Minimum uncertainty wave packet" at all times (an unusual property!)
     * Eigenstate of the destruction operator (strange and important later...)
     * Photon shot noise 
  * The coherent state in the quadrature plane
  * Does the noise of a coherent state increase when it's amplitude increases? 
    * (answer: yes and no...it depends what you measure...)
  * Is it "quantum" or "classical"? (you decide...)

  
#### Lecture 5: Wigner function representation of the wave function

* Definition of the Wigner function representation of a wave function
  * How to calculate it (math)
  * What does it mean?
  * Visualizing how to calculate $\psi(x)$ and $\phi(k)$ from the Wigner function
* Wigner functions of a Fock states and Coherent states
  * Interpretation of photon shot noise in the Wigner plane
* Wigner functions of other interesting quantum states:
  * Squeezed states for reducing quantum noise
     * Quadrature squeezed states
     * Amplitude squeezed states
     * Phase-squeezed states
  * Schroedinger cat states
     * Interpretation of the fringes
     
#### Lecture 6: Collapse noise: what happens if we measure again

* How is quantum sensing different to what you have learned so far?  
  * Expectation values: 
    * Inititalise $\psi$, evolve, measure, throw away, start again, Inititalise $\psi$, evolve, measure, etc
    * Average over all your measurement results
    * But never measure again after you measure! Always re-initialize the wave function $\psi$
  * Sensing something:
    * You don't know what it is!
    * Your objective is to find out what it is
    * You can't "re-initialize it"!
    * What can you do? => Measure again
    * This is not captured by the mathematics of expectation values!!!
  * Challenge: figure out what happens if you measure again after you measure
    * (difficult part: results of measurements are random...)
* Review of what happens after you measure: the generalized statistical interpretation
  * Collapse onto eigenstate of measurement operator corresponding to the observed quantity
  * Outcome is statistically distributed determined by overlap coefficients
  * Additional postulate: if you measure continously, these collapse events occur randomly in time
    * Example: decay of an atom => Poissonian statistics
* Toy example: continuously measuring 0+1 or 0-1 
  * What happens to the wave function if we measure position with an accuracy smaller than the zero point fluctuations? 
  * Example of a the wavefunction during a single experimental realisation (example of a "quantum trajectory")
  * What happens if we average? 
  * What do we do if we don't know the outcome of them measurement? 
  * How do we deal with this in a reasonable way mathematically? (How do we do all the book keeping?)
  * One way: "Quantum trajectories" (MCWF method), which we will come back to at the end of next week :)
  * But first: another tool, the "density matrix" (lecture 7)
  
###  Week 3: How to calculate what happens when we (or someone else) measures

#### Lecture 7: The density matrix
  
* Introduction the density matrix representation
  * "Pure states": Density matrix as an alternative way of writing down
  * "Mixed states": Using the density matrix to mathematically encode your lack of knowledge about a quantum state
* Calculating things with the density matrix
  * Calculating the expectation values of operators
    * Example: spin-1/2 $\langle S_x \rangle$, $\langle S_y \rangle$, $\langle S_z \rangle$
  * Calculating the time evolution of the density matrix
    * Example: spin-1/2 superposition
  * Von-Neumann Entropy: how much do you (still) know about your quantum state?
  
#### Lecture 8: The (Lindblad) Master Equation

* What happens **on average** if we measure continuously?
  * "on average": average over all of the possible quantum trajectories from the random quantum jumps
* **Answer:** The Lindblad master equation
  * Describes the time evolution of the density matrix averaged over all quantum jumps
  * Inputs:
     * initial (optionally pure) density matrix
     * "collapse" operators
     * "measurement rate"
* Example: Decay of the superposition of a spin in a magnetic field by measuring the radiation it emits
  * The answer is easy to understand but the collapse operator is a bit subtle...
  * First: what happens to our spin if it is completely isolated? 
  * What is the collapse operator for measuring "if it has decayed"? 
    * Formally, one can derive this using linearly coupling of the spin to a Harmonic oscillator (provide notes?)
    * Intuitively, a reasonable collapse operator is $S_-$, which will remove an excitation from the spin
  * Solving the Lindblad equation for the decay of a spin
  * Calculating $\langle S_z (t)\rangle$
  
* When should I use quantum trajectories and when should I use the master equation? 
  * Averaged over all possible random measurement outcomes, they give the same result!
  * Difference is what I know and what I measure
    * If I know the outcome of **all** the measurements (if I have 100% efficiency in my measurement chain), then I can get the maximum information quantum mechanics allows about the thing I am measuring for a *single* quantum trajectory 
    * If I do not know then outcomes of any of the measurements ("decoherence", the "environment" is measuring), then the best I can do is average over all the quantum trajectories (master equation)
    * If I have imperfect measurements: I can perform a statistical estimation about what the best estimate is of what the state of the system was conditioned on the information I have (this is called "quantum state estimation", but is beyond the scope of this course...but is not very hard to understand given what we have learned...)


#### Lecture 9: Introduction to QuTiP

* Installing it, loading it, running it
* Where to find Qutip information: Online Lectures, Tutorials, and documentation
* Using it to solve physics problems from this course:
  * Time dependent Schroedinger equation (no measurement)
    * Example: Coherent state
  * Lindblad master equation
    * Example: A decaying coherent state
  * Quantum trajectories: The Monte Carlo Wave Function (MCWF) method
    * Example: Decaying Fock states

### Week 4: Project

In this week, you will work on the Final project. During the scheduled lecture times, the lecturers and assistants will be available in assigned lecture rooms to help with coding and answer questions.
<!-- #endregion -->
