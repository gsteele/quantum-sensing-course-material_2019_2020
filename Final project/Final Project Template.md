---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

<!-- #region {"hide_input": false} -->
# Final Project TN3155: Your title here

## Introduction

A short paragraph or two explaining the aim and scope of your project. Briefly: what is your project about? Which research questions will you answer? What will you try to do? 

Include 1 picture if you want that expresses the thing you are simulating / what you want to achieve. 

## Description of the physical problem being simulated

3-4 paragraphs describing the physical problem that you are simulating with your code. What is the starting point? Do you make some approximations to the Hamiltonian before simulating it? Make some predictions of what you might expect based on your physical intuition (ie. without any derivations / mathematics / code). Try to write this bit in particular from the perspective of what you knew *before* you started the project. 

Include figures: screenshots from papers, textbooks, websites, but making sure to cite your source. 

## Aim and scope of the project

A short, concise list of the questions you will answer using the simulations you will perform.

## Techniques and approach

Outline, in subsections if needed (but one section is fine if you don't!), important details of how you implemented the code. 

You do not have to document in detail each simulation you performed. And also not even the details of what you did for each of the plots in the results section, since the code you will submit will do this for you.

Instead: think about the important concepts / techniques you used and illustrate how each of these works with a simple illustration of how the code works, what it does, and how to use it. 

##  Results

Pick maximum 3 "main results" from what you have done. One result is also fine if your project was focussed on one thing. Formulate them in the form of a "message". What is the "message" you want your reader to "take home"? 

For each of these "message" make a subsection. Write code that produces figures that illustrate your message as clearly as possible. Each subsection should includee: 

* A paragraph describing what the research question the code is trying to answer and, briefly, how it does it
* The code that produces a figure. 
  * If the code is very long, you can "hide" the cell using the "toggle cell input display" notebook extension.
  * Or you can write your code in a function, like make_results_1(), separate .py  file and then  call your  function in the code sell
* A paragraph describing what the figure shows ("what you see")
* A paragraph explaining how to interpret what the figure shows

Each subsection can have multiple plots if you need more than one plot to explain your point. However, I would try to avoid more than 3 plots per "subsection". In getting your message across effectively, "less is more". It is better to have one very effective and clear plot, than a sequence of many plots that will confuse your reader. 

## Summary and Outlook

In a short paragraph or two, summarize again your "take home messages". 

In an additional paragraph, describe what you think would be the next steps: where can we go from here? What would you do if you had more time? Where can one apply the things you have learned? 
<!-- #endregion -->
