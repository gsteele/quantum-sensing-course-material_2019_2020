---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Final Project

## What you should do

* Come up with an idea that interests you
* Come up with a plan how to test it with QuTiP
  * Ask teachers if you're not sure how
* Try some stuff out, see what happens
* Think about what you see: does it make sense? 
* If not: try some more stuff to try to figure it out
* If it does: does what I've learned lead me to new ideas to try? 
* If yes: try them! Repeat cycle

## What you will hand in

A report in the form of a Jupyter Notebook that contains:

* A description of the motivation of the physics problem(s) you want to try to solve
  * Why are these interesting / relevant? 
  * Is there any literature about this?
* Sections for the "final experiments" you did. For each one:
  * A description of what you did and why this address the physics problem you're trying to simulate
  * The code
  * Relevant plots illustrating the results of your "experiment"
    * Each plot should have a "message"! 
    * When making the plots, think carefully about what the "message" is that you want to bring across to the reader, and adjust your plot to illustrate this message as clearly as possible. 
  * An objective discussion of the features of the plot you want to point out to the reader to build up your "case" for convincing them of your interpretation: "showcase the evidence"
  * As separate as possible (different paragraph even sometimes), describe how to intepret these features and lead the reader to the conclusion you draw.
* A final "Conclusions and Outlook" section
  * A concise summary of the most important messages from your work
  * An outlook on what implications these have for understanding / interpreting experiments
  * An outlook about what possible next steps could be

## Topic ideas

General:

* The double slit experiment and destroying interference by measuring "which path" information
* Decoherence from coherence: spontaneous emission and quantum revivals in a non-infinite quantum bath

Spins, spin qubits, and two-level systems:

* Decay, deaphasing, and thermal decoherence of a spin qubit
* Rabi oscillations of a spin qubit coupled to an environment
* Entanglement and Bell correlations in coupled cavities and/or spins
* Measurement-induced dephasing in qubit
* Simulating spin-waves in a chain of quantum spins
* Quantum tunneling and localisation of magnetisation in a macroscopic spin

Superconducting qubits and cavities:

* Superconducting Transmon qubit: a quantum Duffing oscillator
* The role of "quantum" forces in the Rabi oscillations of a transmon qubit
* A transmon qubit as a photon-number-resolving detector
* Superconducting quantum amplifiers
* Vacuum Rabi oscillations between two quantum oscillators / qubits
* Creating cat states in a nonlinear cavity


