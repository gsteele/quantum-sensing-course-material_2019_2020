{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  Project: Quantum Jumps of Light"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this project, we will explore simulating and understanding experiments from the group of Serge Haroche on the observing quantum jumps of light. The two experiments we will look at are this one:\n",
    "\n",
    "*Quantum jumps of light recording the birth and death of a photon in a cavity* <br>\n",
    "Sébastien Gleyzes, Stefan Kuhr, Christine Guerlin, Julien Bernu, Samuel Deléglise, Ulrich Busk Hoff, Michel Brune, Jean-Michel Raimond & Serge Haroche <br>\n",
    "Nature **446**, 297 (2007) <br>\n",
    "https://www.nature.com/articles/nature05589\n",
    "\n",
    "and this one:\n",
    "\n",
    "*Progressive field-state collapse and quantum non-demolition photon counting*  <br>\n",
    "Christine Guerlin, Julien Bernu, Samuel Deléglise, Clément Sayrin, Sébastien Gleyzes, Stefan Kuhr, Michel Brune, Jean-Michel Raimond & Serge Haroche <br>\n",
    "Nature **448**, 889 (2007) <br>\n",
    "https://www.nature.com/articles/nature06057\n",
    "\n",
    "In these experiments, published at nearly the same time, they use a quantum-non-demolition (QND) measurement of the photon number to observe quantum jumps by continuously monitoring the states of the cavity. In this project, we will try to explore these concepts using the Lindblad master equation in [QuTiP](http://qutip.org/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Description of the physical problem we want  to simulate\n",
    "\n",
    "In the experiment, they use Rydberg atoms to detect the state of photons in a 50 GHz microwave cavity,  illustrated here:"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](fig1.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will not focus in detail here on how they use the Rydberg atoms to detect the cavity photon number: we will not try to simulate the protocol (which we could do if we  want in QuTiP), but instead assume that their atoms are equivalent to a measurement of photon number.\n",
    "\n",
    "Briefly, though, the idea is that atoms are sent \"flying\" through the cavity $C$ from the left to the right in the picture. The atoms are excited into a large quantum  number Rydberg state, which dramatically increases their diople moment and their coupling to electromagnetic fields. The atom itself has a large number of quantum  states, but  in these experiments, only two will be relevant: the states the atoms start in, which we will label $|0\\rangle$ and an excited  state of the atom $|1\\rangle$. \n",
    "\n",
    "Before they enter the cavity, they are excited into a superposition of $|0\\rangle$ and $|1\\rangle$ by the \"Ramsey cavity\" $R_1$. They then enter the cavity $C$,  where they interact with the photons in the cavity, acquiring a phase depending on the number of photons in the cavity. After interacting with the cavity,  they  come out the other side,  where they undergo another excitation in cavity  $R_2$. If the phase that that atom acquires in the cavity is just right, it will then be excited with 100% probability to the excited state $|1\\rangle$, which can be detected by an \"ionizing\" detector $D$ with relatively high efficiency and not too many false counts. Key to this detection scheme is that the transition with which the atom is excited is not resonant with the cavity $C$: because of this, the atom  does not decay into the cavity, but instead acquires a phase dependent on the state of the cavity. These non-resonant atoms cannot add or subtract photons from the cavity, an  important distinction from earlier work based on detecting photons by absorbing them. \n",
    "\n",
    "If they do not do anything to the cavity, it is in a thermal state with an average photon number of 0.063: it will mostly  be in the ground state, but every so often, it will be excited to a single photon state  by  a  thermal  fluctuation.  By continously measuring the state of the cavity with the atoms, the researchers are able to directly observe the quantum jumps of the \"birth\" and the \"death\" of thermal photons:"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](fig2.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They also are able to \"excite\" the cavity by sending in an atom that is excited with a transition that is intentionally  resonant with the cavity. Doing this carefully,  they can prepare the cavity deterministically with exactly one photon,  and then use non-resonant atoms to watch it decay.  \n",
    "\n",
    "In a single run  of the experiment, what they observe is that at a random time after exciting the cavity with one photon, this photon decays. Repeating this experiment many times,  and then \"ensemble\"  averaging them, the recover the exponential decay predicted by the Lindblad master equation:"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](fig3.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a second experiment, they instead excited the cavity with a coherent state (by \"blasting\" microwaves into the setup from a classical microwave generator) and then watched these this coherent state decay one photon at a time:\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](fig4.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in  this experiment, they observe \"continuous\" transition between the average photon numbers that they measure, leading to the question: are these quantum jumps instantaneous or not? And if why not, what  does that mean? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Aim and scope of this project\n",
    "\n",
    "In  this project, we aim to simulate these results using the  Lindblad master equation in QuTiP. The questions we want to answer are:\n",
    "\n",
    "1. What do we need in our theory to reproduce the observation of the \"birth\"  and  \"death\" of a  thermal phonon? \n",
    "\n",
    "2. Can we simulate the reconstruction of the \"decay curve\" of the single photon?\n",
    "\n",
    "3. Can we understand more about the origins of the \"continous\" quantum  jumps? Is this fundamental or an experimental  limitation? Can we  simulate this in QuTiP? \n",
    "\n",
    "4. How does the  measurement affect the state we are observing? What kind of quantum trajectories do we observe without the photon number monitoring? And does this  depend on  the  state we start in? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Techniques and approach\n",
    "\n",
    "To answer these questions, we will approach the problem of simulating the state of the photons under the condition of rapid continuous measurement, corresponding to a collapse operator given by the photon number operator $\\hat n$, and also under coupling to an environment via an  annihilation  operator $\\hat a$.  \n",
    "\n",
    "For ensemble averages, we will use the `mesolve()` routine. To observe a single quantum trajectory, as is possible in the experiment above due to the unique measurement apparatus,  we will use the Monte Carlo solver `mcsolve()` and examine the results from a single trajectory.  \n",
    "\n",
    "We will also explore the influence\n",
    "\n",
    "### Modelling  photon  measurment\n",
    "\n",
    "...\n",
    "\n",
    "### Modelling decay\n",
    "\n",
    "...\n",
    "\n",
    "### Modelling  thermal occupation\n",
    "\n",
    "...\n",
    "\n",
    "### Using the Monte Carlo and Master Equation solver routines\n",
    "\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Results\n",
    "\n",
    "...\n",
    "\n",
    "### Observing quantum jumps of  thermal photons\n",
    "\n",
    "...\n",
    "\n",
    "### Reproducing decay curves from ensembles of  measurements\n",
    "\n",
    "...\n",
    "\n",
    "### Quantum trajectories of a monitored coherent state\n",
    "\n",
    "...\n",
    "\n",
    "### Quantum trajectories of a coherent state with no photon number monitoring\n",
    "\n",
    "...a surprise!...(for gary at least?)...and  when you think  about it, it makes sense..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook  \n",
    "\n",
    "Here we performed so and so simulations and saw so and so results...this result explains this and maybe predicts that...etc...etc... (1-2 paragraphs  max!)\n",
    "\n",
    "Our result suggest that so and so future experiments or simulations would be interesting to perform...etc.etc. (1-2 paragraphs max!)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
