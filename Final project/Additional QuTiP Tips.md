---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
from qutip import *
import numpy as np
import matplotlib.pyplot as plt
```

# Additional  QuTiP tips / FAQ

I will update this as the course goes on with various tips on how to do things in QuTiP. 

## Creating  a density matrix from a ket

Mathematically, the definition of the density matrix of a pure state with wavefunction $|\psi\rangle$ is:

$$
\rho = |\psi\rangle \langle\psi|
$$

Mathematically, the "bra" $\langle \psi|$  is the Hermitian conjugate of   $|\psi\rangle$, and so  we  can  also  write  this as:

$$
\rho = |\psi\rangle (|\psi\rangle)^\dagger
$$

This is also exactly how we  do it  in qutip:

```python
psi  = fock(3,0) + fock(3,2)
rho  = psi * psi.dag()
```

```python
psi
```

```python
rho
```
